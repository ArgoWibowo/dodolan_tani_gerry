<?php 
include 'config/class.php';

?>

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Login</title>
</head>
<link rel="stylesheet" href="asset/font-awesome/css/font-awesome.min.css">
<link rel="stylesheet" href="asset/css/bootstrap.min.css">
<link rel="stylesheet" href="asset/css/login.css">
<body class="index">
	<div class="container">
		<div class="row">
			<div class="col-md-4 col-md-offset-4">
				<div class="kotak-login-index">
					<form method="post" ="">
						<legend class="text-center"><i class="fa fa-lock"></i> Login Dodolantani</legend>
						<div class="form-group">
							<label for="">Email</label>
							<input type="email" class="form-control" name="email">
						</div>
						<div class="form-group">
							<label for="">Password</label>
							<input type="Password" class="form-control" name="pass">
						</div>
						<center><button class="btn btn-primary" name="login">Login</button></center>
					</form>
					<br>
					<?php 
					if (isset($_POST['login']))
					{
						$hasil= $admin->login_admin($_POST['email'], $_POST['pass']);
						if ($hasil=="sukses")
						{
							echo "<div class='alert alert-info'>Sukses, Anda masuk admin</div>";
							echo "<meta http-equiv='refresh' content='1;url=admin/index.php'>";
						}
						else
						{
							$hasil= $penjual->login_penjual($_POST['email'], $_POST['pass']);
							if ($hasil=="sukses")
							{
								echo "<div class='alert alert-info'>Login sukses</div>";
								echo "<meta http-equiv='refresh' content='1;url=penjual/index.php'>";
							}
							else
							{
								echo "<div class='alert alert-danger'>Gagal login</div>";
								echo "<meta http-equiv='refresh' content='1;url=login.php'>";
							}
						}
					}

					?>
				</div>
			</div>
		</div>
	</div>
</body>
</html>