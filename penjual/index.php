<?php 
include  "../config/class.php";
?>

<?php 
if(!isset($_SESSION['penjual']))
{
  echo "<script>alert('Anda harus login'); </script>";
  echo "<script>location='login.php'; </script>";
}

$iduser = $_SESSION['penjual']['ID_User'];
// $datakat = $kategori->tampil_kategori();
// foreach ($data as $key => $value)
// {
// }
$laporan_hasil = $penjual->laporan_penjualan_perkategori('Kat3', date('m'), date("Y"), $iduser);
$laporan_bahan = $penjual->laporan_penjualan_perkategori('Kat2', date('m'), date("Y"), $iduser);
$laporan_alat = $penjual->laporan_penjualan_perkategori('Kat1', date('m'), date("Y"), $iduser);

$hasil = $penjual->pesan_belum_dibaca();
?>

<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>Penjual</title>
  <link href="../asset/css/bootstrap.min.css" rel="stylesheet">
  <link rel="stylesheet" href="../asset/css/stylepenjual.css">
  <link rel="stylesheet" href="../asset/font-awesome/css/font-awesome.css">
  <link rel="stylesheet" href="https://cdn.datatables.net/1.10.12/css/dataTables.bootstrap.min.css">
</head>
<body>

  <div id="wrapper">
    <nav class="navbar navbar-default hidden-print">
      <div class="navbar-header">
        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target=".sidebar-collapse" aria-expanded="false">
          <span class="sr-only">Toggle navigation</span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
        </button>
        <a class="navbar-brand" href="index.php">DODOLANTANI</a>
      </div>
      <ul class="nav navbar-nav navbar-right" style="margin-right: 20px;">
        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown">Hello <?php echo $_SESSION['penjual']['nama']; ?></a>
          <ul class="dropdown-menu">
            <li><a href="index.php?halaman=logout"><i class="fa fa-sign-out"></i> Log Out</a></li>
          </ul>
        </li>
      </ul>
      <!-- <ul class="nav navbar-nav navbar-right">
      <li><a href="#">Link</a></li></ul> -->
    </nav>

    <nav class="navbar-default navbar-side hidden-print">
      <div class="sidebar-collapse">
        <ul class="nav" id="main-menu">
          <li><a href="index.php"><i class="fa fa-home"></i> Home</a></li>
          <li><a href="index.php?halaman=produk"><i class="fa fa-tags"></i> Lihat Daftar Produk</a></li>
          <li><a href="index.php?halaman=cek_harga"><i class="fa fa-product-hunt"></i> Cek Harga Produk </a></li>

          <li>
            <a data-toggle="collapse" data-target="#penjualan"><i class="fa fa-product-hunt"></i> Penjualan</a>
            <ul class="nav collapse" id="penjualan">
              <li>
                <a href="index.php?halaman=permintaan"><i class="fa fa-product-hunt"></i> Daftar Transaksi Penjualan</a>
              </li>
              <li>
                <a href="index.php?halaman=cek_pengiriman"><i class="fa fa-product-hunt"></i> Cek Status Pemesanan</a>
              </li>
            </ul>
          </li>

          <li>
           <a data-toggle="collapse" data-target="#kotakmasuk"><i class="fa fa-user"></i> Kotak Masuk <!-- <span class="badge"><?php echo count($hasil); ?> --></span></a>
           <ul class="nav collapse" id="kotakmasuk">
            <li>
              <a href="index.php?halaman=diskusi"><i class="fa fa-user"></i> Diskusi Produk</a>
            </li>
            <li>
              <a href="index.php?halaman=ulasan"><i class="fa fa-user"></i> Ulasan Produk</a>
            </li>
          </ul>
        </li>

        <li><a href="index.php?halaman=tambah_produk"><i class="fa fa-plus"></i> Tambah Produk</a></li>

        <li>
         <a data-toggle="collapse" data-target="#laporan"><i class="fa fa-table"></i> Laporan</a>
         <ul class="nav collapse" id="laporan">
          <li>
            <a href="index.php?halaman=laporan_transaksi"><i class="fa fa-table"></i> Transaksi</a>
          </li>
          <li>
            <a href="index.php?halaman=laporan_produk"><i class="fa fa-table"></i> Produk Terjual</a>
          </li>
        </ul>
      </li>

      <li><a href="index.php?halaman=profil&id=<?php echo $_SESSION['penjual']['ID_User']; ?>"><i class="fa fa-user"></i> Profil Saya</a></li>
      <!-- <li><a href="index.php?halaman=logout"><i class="fa fa-sign-out"></i> Log Out</a></li> -->
    </ul>
  </div>
</nav>
<div id="page-wrapper">
  <div id="page-inner">
    <?php 
        //jika tidak ada parameter halaman maka
    if(!isset($_GET['halaman']))
    {
      include 'home.php';
    }
    else
    {
      if ($_GET['halaman']=="logout") 
      {
        include 'logout.php';
      }
      elseif($_GET['halaman']=="produk")
      {
        include 'produk/tampil_produk.php';
      }
      elseif($_GET['halaman']=="ubah_produk")
      {
        include 'produk/ubah_produk.php';
      }
      elseif($_GET['halaman']=="ubah_produk_alat")
      {
        include 'produk/ubah_produk_alat.php';
      }
      elseif($_GET['halaman']=="ubah_produk_bahan")
      {
        include 'produk/ubah_produk_bahan.php';
      }
      elseif($_GET['halaman']=="hapus_produk")
      {
        include 'produk/hapus_produk.php';
      }
      elseif($_GET['halaman']=="hapus_produk_alat")
      {
        include 'produk/hapus_produk_alat.php';
      }
      elseif($_GET['halaman']=="hapus_produk_bahan")
      {
        include 'produk/hapus_produk_bahan.php';
      }
      elseif($_GET['halaman']=='cek_harga')
      {
        include 'produk/cek_harga_produk.php';
      }
      elseif($_GET['halaman']=='tambah_produk')
      {
        include 'produk/tambah_produk.php';
      }
      elseif($_GET['halaman']=='tambah_penawaran')
      {
        include 'penawaran/tambah_penawaran.php';
      }
      elseif($_GET['halaman']=='permintaan')
      {
        include 'permintaan/tampil_permintaan.php';
      }
      elseif($_GET['halaman']=='profil')
      {
        include 'profil_saya.php';
      }
      elseif($_GET['halaman']=='cek_pengiriman')
      {
        include 'permintaan/cek_pengiriman.php';
      }
      elseif($_GET['halaman']=='diskusi')
      {
        include 'kotak_masuk/diskusi_produk.php';
      }
      elseif($_GET['halaman']=='isi')
      {
        include 'kotak_masuk/isi_diskusi.php';
      }
      elseif($_GET['halaman']=='tidak_aktif')
      {
        include 'produk/tidak_aktif_produk.php';
      }
      elseif($_GET['halaman']=='aktif')
      {
        include 'produk/aktif_produk.php';
      }
      elseif($_GET['halaman']=='ulasan')
      {
        include 'kotak_masuk/ulasan_produk.php';
      }
      elseif($_GET['halaman']=='laporan_transaksi')
      {
        include 'laporan/laporan_transaksi_jual.php';
      }
      elseif ($_GET['halaman']=='isi_ulasan')
      {
        include 'kotak_masuk/isi_ulasan.php';
      }
      elseif ($_GET['halaman']=='laporan_produk')
      {
        include 'laporan/laporan_transaksi_produk.php';
      }
      elseif ($_GET['halaman']=='cek_transfer')
      {
        include 'permintaan/cek_transfer_admin.php';
      }
    }

    ?>

  </div>
</div>
</div>



<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script src="../asset/js/bootstrap.min.js"></script>
<script src="../asset/js/mobile.js"></script>
<script src="https://cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.12/js/dataTables.bootstrap.min.js"></script>
<script src="../asset/js/table.js"></script>
<script src="https://code.highcharts.com/highcharts.js"></script>
<script src="https://code.highcharts.com/modules/exporting.js"></script>
<script type="text/javascript">Highcharts.chart('container', {
  chart: {
    plotBackgroundColor: null,
    plotBorderWidth: null,
    plotShadow: false,
    type: 'pie'
  },
  title: {
    text: 'Laporan Penjualan Produk Per Kategori <?php echo date("F"); ?> <?php echo date("Y"); ?>'
  },
  tooltip: {
    pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
  },
  plotOptions: {
    pie: {
      allowPointSelect: true,
      cursor: 'pointer',
      dataLabels: {
        enabled: true,
        format: '<b>{point.name}</b>: {point.percentage:.1f} %',
        style: {
          color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
        }
      }
    }
  },
  series: [{
    name: 'Brands',
    colorByPoint: true,
    data: [{
      name: 'Produk Kategori Alat',
      y: <?php echo count($laporan_alat); ?>,
      sliced: true,
      selected: true
    }, {
      name: 'Produk Kategori Bahan',
      y: <?php echo count($laporan_bahan); ?>
    }, {
      name: 'Produk Kategori Hasil',
      y: <?php echo count($laporan_hasil); ?>
    }]
  }]
});
</script>
</body>
</html>