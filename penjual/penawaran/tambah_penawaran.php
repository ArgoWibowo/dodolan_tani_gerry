<?php
$akun = $_SESSION['penjual'];

$id_produk = $_GET['kode'];

$ambil_data = $produk->ambil_produk_ditawarkan($id_produk);

if (isset($ambil_data['Nama_Hasil']))
{
	$ambil['nama_barang'] = $ambil_data['Nama_Hasil'];
}
elseif (isset($ambil_data['Nama_Alat']))
{
	$ambil['nama_barang'] = $ambil_data['Nama_Alat'];
}
elseif(isset($ambil_data['Nama_Bahan']))
{
	$ambil['nama_barang'] = $ambil_data['Nama_Bahan'];	
}

$kode_produk = $ambil_data['ID_Produk'];

?>

<!-- <pre>
	<?php print_r($ambil_data); ?>
</pre> -->

<h2>Masukan Detail Produk</h2>
<form method="post" enctype="multipart/form-data">
	<div class="form-group">	
		<label>Nama Barang</label>
		<input type="text"  class="form-control" name="nama_barang"
		value="<?php echo $ambil['nama_barang']; ?>" readonly>
	</div>
	<div class="form-group">	
		<label>Spesifikasi Barang</label>
		<textarea class="form-control" name="spesifikasi_barang" required=""></textarea>
	</div>
	<div class="form-group">
		<label>Kondisi Barang</label>
		<select class="form-control" name="kondisi_barang">
			<option>--Pilih Kondisi--</option>
			<option value="Baru">Baru</option>
			<option value="Bekas">Bekas</option>
		</div>
	</select>
	<br>
	<div class="form-group">
		<label>Merk</label>
		<input type="text"  class="form-control" name="merk">
	</div>
	<div class="form-group">
		<label>Harga</label>
		<input type="text"  class="form-control" name="harga" value=" <?php echo $ambil_data['Harga']; ?>">
	</div>
	<div class="form-group">
		<label>Tahun Produksi</label>
		<input type="text"  class="form-control" name="thn_produksi">
	</div>
	<div class="form-group">
		<label>Gambar 1</label>
		<input type="file"  class="form-control" name="gambar1">
	</div>
	<div class="form-group">
		<label>Gambar 2</label>
		<input type="file"  class="form-control" name="gambar2">
	</div>
	<div class="form-group">
		<label>Stok</label>
		<input type="number"  class="form-control" name="stok">
	</div>
	<div class="form-group">
		<label>Satuan Barang</label>
		<input type="text"  class="form-control" name="satuan_barang">
	</div>
	<button class="btn btn-warning" name="simpan">Simpan</button>
</form>

<?php 
if(isset($_POST['simpan']))
{

	$hasil = $produk->simpan_penawaran_produk($kode_produk, $_POST['spesifikasi_barang'], $_POST['kondisi_barang'], $_POST['merk'], $_POST['thn_produksi'], $_FILES['gambar1'], $_FILES['gambar2'], $_POST['stok'], $_POST['satuan_barang'], $akun['ID_User']);
	if($hasil=="sukses")
	{

		echo "<script>alert('Data masuk Penjualan, Silahkan Tunggu validasi admin');</script>";
		echo "<script>location='index.php?halaman=produk';</script>";
	}
	else
	{
		echo "<script>alert('Gagal, file yang diuplod bukan foto');</script>";
		echo "<script>location='index.php?halaman=tambah_upload_produk';</script>";
	}
}

?>