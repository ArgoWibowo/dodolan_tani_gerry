<title>Data Lengkap</title>
<div class="container">
	<h2>Masukan Data Diri</h2>
	<br>
	<form method="post" enctype="multipart/form-data">
		<div>
			<label>Jenis Kelamin</label>
			<select name="jeniskelamin" class="form-control">
				<option value="">--Pilih Jenis Kelamin--</option>
				<option value="1">laki-laki</option>
				<option value="0">perempuan</option>
			</select>
		</div>
		<br>
		<div class="form-group">
			<label>Tanggal Lahir</label>
			<input type="date" class="form-control" name="tgl_lhr">
		</div>
		<div class="form-group">
			<label>Alamat</label>
			<textarea class="form-control" name="alamat" ></textarea>
		</div>
		<div class="form-group">
			<label>Provinsi</label>
			<input type="text" class="form-control" name="provinsi">
		</div>
		<div class="form-group">
			<label>Kabupaten</label>
			<input type="text" class="form-control" name="kabupaten">
		</div>
		<div class="form-group">
			<label>Kecamatan</label>
			<input type="text" class="form-control" name="kecamatan">
		</div>
		<div class="form-group">
			<label>Kelurahan</label>
			<input type="text" class="form-control" name="kelurahan">
		</div>
		<div class="form-group">
			<label>Nomor Telepon</label>
			<input type="number" class="form-control" name="no_tlpn">
		</div>
		<div class="form-group">
			<label>Foto</label>
			<input type="file" class="form-control" name="foto">
		</div>
		<center><button class="btn btn-success" name="daftar">Daftar</button></center>
		<br>
		<br>
	</form>

	<?php 
		if(isset($_POST['daftar']))
		{
			$hasil = $user->simpan_detail_user($_GET['id'],$_POST['jeniskelamin'], $_POST['tgl_lhr'], $_POST['alamat'], $_POST['provinsi'], $_POST['kabupaten'], $_POST['kecamatan'], $_POST['kelurahan'], $_POST['no_tlpn'], $_FILES['foto']);

			if($hasil=="sukses")
			{
				echo "<script>location='index.php?halaman=pemberitahuan';</script>";
			}
			else
			{
				echo "<script>alert('Gagal, file yang diuplod bukan foto');</script>";
				echo "<script>location='index.php?halaman=data_lengkap';</script>";
			}	
		}
			?>

</div>

