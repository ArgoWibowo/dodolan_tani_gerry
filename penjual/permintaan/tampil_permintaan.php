<?php 
if (isset($_GET['status_kirim'])) 
{
	$pembeli->ubah_status_kirim($_GET['status_kirim']);
	echo "<script>alert('Konfirmasi Pengiriman Berhasil');</script>";
	echo "<script>location='index.php?halaman=permintaan';</script>";
}

$ID_User= $_SESSION['penjual']['ID_User'];


// jika ada select di dalam form dengan nama cari maka
if (isset($_POST['cari'])) 
{
	$cari = $_POST['cari'];
	if($_POST['cari']=="semua")
	{
		$data_cari=$pembeli->tampil_transaksi_beli($ID_User);
	}
	else
	{

		$data_cari = $pembeli->cari_berdasarakan_sorting($_POST['cari'], $ID_User);
	}
}
else
{
	$data_cari=$pembeli->tampil_transaksi_beli($ID_User);
	$cari="";

	$data_produk['Nama_Hasil']="";
	$data_produk['Nama_Alat']="";
	$data_produk['Nama_Bahan']="";
	$data_produk['Deskripsi_Hasil']='';
	$data_produk['Deskripsi_ALat']='';
	$data_produk['Deskripsi_Bahan']='';

}
?>
<h2>Daftar Transaksi Penjualan</h2>
<form method="post" class="form-inline">
	<div class="form-group">
		<select class="form-control" name="cari" onchange="submit()">
			<option value="semua">Semua Transaksi</option>
			<option value="Belum Bayar" <?php if($cari=="Belum Bayar"){echo "selected";} ?>>Belum Bayar</option>
			<option value="Sudah Bayar" <?php if($cari=="Sudah Bayar"){echo "selected";} ?>>Sudah Bayar</option>
			<option value="Belum di Kirim" <?php if($cari=="Belum di Kirim"){echo "selected";} ?>>Belum di Kirim</option>
			<option value="Sudah Kirim" <?php if($cari=="Sudah Kirim"){echo "selected";} ?>>Sudah Kirim</option>
		</select>
	</div>
</form>
<br>
<div class="table-responsive"> 
	<table class="table table-bordered table-striped" id="#">
		<thead>
			<tr>
				<th>PRODUK</th>
				<th>NO INVOICE</th>
				<th>KODE PENAWARAN</th>
				<th>QTY</th>
				<th>HARGA</th>
				<th>TANGGAL KEBUTUHAN</th>
				<th>TANGGAL PERMINTAAN</th>
				<th>EXPEDISI</th>
				<th>NO RESI</th>
				<th colspan="3" ="3"><center>STATUS</center></th>
			</tr>
		</thead>
		<tbody>
			<?php foreach ($data_cari as $key => $value): ?>
				<?php //if ($value['Status']=="Sudah Bayar" AND $value['Status_Kirim']=="Sudah Kirim"): ?>
				<?php //else: ?>
					<tr>
						<td><center><img src="../asset/img/produk/<?php echo $value['Gambar1']; ?>" width="50" data-toggle="modal" data-target="#myModal<?php echo $value['ID_Produk']; ?>"></td></center> 
						<td> <?php echo $value['ID_Permintaan']; ?></td>
						<td> <?php echo $value['ID_Penawaran']; ?></td>
						<td> <?php echo $value['Qty']; ?></td>
						<td> <?php echo $value['Harga']; ?></td>
						<td> <?php echo $value['Tgl_Kebutuhan']; ?></td>
						<td> <?php echo $value['Tgl_Permintaan']; ?></td>
						<td> <?php echo $value['Nama_Ekspedisi']; ?></td>
						<td> <?php echo $value['No_Resi']; ?></td>
						<td> <?php echo $value['Status']; ?></td>
						<td> <?php echo $value['Status_Kirim']; ?></td>
						<?php if ($value['Status_Kirim']=='Sudah Kirim'): ?>
							<td>
								<a href="index.php?halaman=cek_transfer&id=<?php echo $value['ID_Permintaan']; ?>" class="btn btn-warning btn-block">Cek Transfer</a>
							</td>
						<?php else: ?>
							<td><center>
								<?php if (empty($value['Status'])): ?>
								<?php elseif($value['Status']=="Belum Bayar"): ?>
								<?php else: ?>
									<a href="index.php?halaman=permintaan&status_kirim=<?php echo $value['ID_Permintaan']; ?>" class="btn btn-primary btn-block">Konfirmasi Pengriman</a></center><br>
								<?php endif ?>
							</td>
						<?php endif ?>
					</tr>
				<?php //endif ?>
			<?php endforeach ?>
		</tbody>
	</table>
</div>


<!-- Modal -->
<?php foreach ($data_cari as $key => $value): ?>
	<?php
	$ambil = $produk->ambil_penawaran_produk($value['ID_Produk']);
	$data_produk = $produk->ambil_produk_penawaran($ambil['ID']);

	?>
	<div class="modal fade" id="myModal<?php echo $value['ID_Produk'] ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				</div>
				<div class="modal-body">
					<center><img src="../asset/img/produk/<?php echo $value['Gambar1']; ?>" class="img-responsive"></center>
					<br>
					<table class="table">
						<tr>
							<th>Nama Produk</th>
							<td>
								<?php
								if(!empty($data_produk['Nama_Hasil']))
								{
									echo $data_produk['Nama_Hasil'];
								}
								elseif(!empty($data_produk['Nama_Alat']))
								{
									echo $data_produk['Nama_Alat'];	
								}
								elseif(!empty($data_produk['Nama_Bahan']))
								{
									echo $data_produk['Nama_Bahan'];
								}
								?>
							</td>
						</tr>
						<tr>
							<th>Harga Produk</th>
							<td> <?php echo $value['Harga']; ?></td>
						</tr>
						<tr>
							<th>Satuan </th>
							<td> <?php echo $value['Satuan_Barang']; ?></td>
						</tr>
						<tr>
							<th>Spesifikasi Barang </th>
							<td> <?php echo $value['Spesifikasi_Barang']; ?></td>
						</tr>
						<tr>
							<th>Deskripsi</th>
							<td> 
								<?php
								if(!empty($data_produk['Deskripsi_Hasil']))
								{
									echo $data_produk['Deskripsi_Hasil'];
								}
								elseif(!empty($data_produk['Deskripsi_ALat']))
								{
									echo $data_produk['Deskripsi_ALat'];	
								}
								elseif(!empty($data_produk['Deskripsi_Bahan']))
								{
									echo $data_produk['Deskripsi_Bahan'];
								}
								?>
							</td>
						</tr>
						<tr>
							<th>Kondisi Barang </th>
							<td> <?php echo $value['Kondisi_Barang']; ?></td>
						</tr>
						<tr>
							<th>Merk </th>
							<td> <?php echo $value['Merk']; ?></td>
						</tr>
						<tr>
							<th>Tahun Produksi </th>
							<td> <?php echo $value['Tahun_Produksi']; ?></td>
						</tr>
					</table>
				</div>
			</div>
		</div>
	</div>
<?php endforeach ?>
