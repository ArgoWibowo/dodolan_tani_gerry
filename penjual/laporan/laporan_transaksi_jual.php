<?php 
//jika ada parameter status_kirim di url maka, 

if (isset($_GET['status_kirim'])) 
{
	$pembeli->ubah_status_kirim($_GET['status_kirim']);
	echo "<script>alert('Konfirmasi Pengiriman Berhasil');</script>";
	echo "<script>location='index.php?halaman=permintaan';</script>";
}
 $ID_User= $_SESSION['penjual']['ID_User'];

$data_trans = $pembeli->tampil_transaksi_beli($ID_User)

?>
<h2>Laporan Transaksi Penjualan</h2>
<form method="post" class="form-inline hidden-print">
	<input type="date" id="tgl1" name="date1" class="form-control">  s.d
	<input type="date" id="tgl2" name="date2" class="form-control">
	<button class="btn btn-success" name="cari"><span class="glyphicon glyphicon-search"</span></button>
	<button class="btn btn-success" name="semua_trans">Semua Transaksi</button>
</form>
<?php
if(isset($_POST['cari']))
{
	$data_trans = $pembeli->cari_transaksi($_POST['date1'], $_POST['date2'],$ID_User);
}
elseif (isset($_POST['semua_trans'])) 
{
	$data_trans = $pembeli->tampil_transaksi_beli($ID_User);
}
?>
<br>
<table class="table table-bordered table-striped" id="#">
	<thead>
		<tr>
			<th>NO</th>
			<th>KODE PEMBELI</th>
			<th>NO INVOICE</th>
			<th>QTY</th>
			<th>HARGA</th>
			<th>TANGGAL KEBUTUHAN</th>
			<th>TANGGAL PERMINTAAN</th>
			<th>EKPEDISI</th>
			<th>NO RESI</th>
		</tr>
	</thead>
	<tbody>
		<?php foreach ($data_trans as $key => $value): ?>
			<tr>
				<td> <?php echo $key+1; ?></td>
				<td> <?php echo $value['ID_User']; ?></td>
				<td> <?php echo $value['ID_Permintaan']; ?></td>
				<td> <?php echo $value['Qty']; ?></td>
				<td> <?php echo $value['Harga']; ?></td>
				<td> <?php echo $value['Tgl_Kebutuhan']; ?></td>
				<td> <?php echo $value['Tgl_Permintaan']; ?></td>
				<td> <?php echo $value['Nama_Ekspedisi']; ?></td>
				<td> <?php echo $value['No_Resi']; ?></td>
			</tr>
		<?php endforeach ?>
	</tbody>
</table>
<a onclick="window.print()" class="btn btn-success hidden-print">Cetak And Save</a>