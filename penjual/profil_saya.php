<?php 

$ambil_id = $_GET['id'];

$data_user = $user->ambil_detail_user($ambil_id);

?>

<div class="row">
	<div class="col-md-7 ">
		<div class="panel panel-default">
			<div class="panel-heading"> <h4 >Data Saya</h4></div>
			<div class="panel-body">
				<div class="box box-info">
					<div class="box-body">
						<div class="col-sm-6">
							<div  align="center"> <img alt="User Pic" src="../asset/img/foto/<?php echo $data_user['Foto'];?>" class="img-square img-responsive" width="80" "> 
								<input id="profile-image-upload" class="hidden" type="file">
								<div style="color:#999;" >Upload Foto</div>
								<!--Upload Image Js And Css-->

							</div>

							<br>

							<!-- /input-group -->
						</div>
						<div class="clearfix"></div>
						<hr style="margin:5px 0 5px 0;">


						<div class="col-sm-5 col-xs-6 tital " >Nama Lengkap</div><div class="col-sm-7 col-xs-6 "> <?php echo $data_user['nama']; ?></div>
						<div class="clearfix"></div>
						<div class="bot-border"></div>

						<div class="col-sm-5 col-xs-6 tital " >Jenis Kelamin</div><div class="col-sm-7"> <?php echo $data_user['jenis_kelamin']; ?></div>

						<div class="clearfix"></div>
						<div class="bot-border"></div>

						<div class="col-sm-5 col-xs-6 tital " >Tanggal Lahir</div><div class="col-sm-7"> <?php echo $data_user['tanggal_lahir']; ?></div>

						<div class="clearfix"></div>
						<div class="bot-border"></div>

						<div class="col-sm-5 col-xs-6 tital " >Alamat</div><div class="col-sm-7"> <?php echo $data_user['alamat']; ?></div>

						<div class="clearfix"></div>
						<div class="bot-border"></div>
						<div class="col-sm-5 col-xs-6 tital " >Provinsi</div><div class="col-sm-7"><?php echo $data_user['provinsi']; ?></div>

						<div class="clearfix"></div>
						<div class="bot-border"></div>
						<div class="col-sm-5 col-xs-6 tital " >Kabupaten</div><div class="col-sm-7"><?php echo $data_user['kabupaten']; ?></div>

						<div class="clearfix"></div>
						<div class="bot-border"></div>

						<div class="col-sm-5 col-xs-6 tital " >Kecamatan</div><div class="col-sm-7"><?php echo $data_user['kecamatan']; ?></div>

						<div class="clearfix"></div>
						<div class="bot-border"></div>

						<div class="col-sm-5 col-xs-6 tital " >Kelurahan:</div><div class="col-sm-7"> <?php echo $data_user['kelurahan_desa']; ?></div>

						<div class="clearfix"></div>
						<div class="bot-border"></div>

						<div class="col-sm-5 col-xs-6 tital " >Nomor Telepon</div><div class="col-sm-7"><?php echo $data_user['nomor_telpon']; ?></div>

						<div class="clearfix"></div>
						<div class="bot-border"></div>

						<div class="col-sm-5 col-xs-6 tital " >Email</div><div class="col-sm-7"> <?php echo $data_user['Email']; ?></div>

						<div class="clearfix"></div>
						<div class="bot-border"></div>
					</div>
				</div>
			</div>
		</div>
		<script>
			$(function() {
				$('#profile-image1').on('click', function() {
					$('#profile-image-upload').click();
				});
			});       
		</script> 
	</div>
</div>




