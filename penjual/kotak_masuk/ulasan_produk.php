<?php 
$dataproduk['Nama_Hasil']="";
$dataproduk['Nama_Alat']="";
$dataproduk['Nama_Bahan']="";

$id_user = $_SESSION['penjual']['ID_User'];


$dataproduk = $produk->tampil_penawaran_produk_user($id_user);

?>

<h3>Ulasan Produk</h3>
<br>
<div class="row">
	<?php foreach ($dataproduk as $key => $value): ?>
		<?php $data = $produk->ambil_penawaran_produk($value['ID_Produk']); ?>
		<?php $data_produk = $produk->ambil_produk_penawaran($data['ID']); ?>
		<div class="col-md-4">
			<div class="thumbanil">
				<center><img src="../asset/img/produk/<?php echo $value['Gambar1']; ?>" class="img-responsive" width="150"></center><br>
				<div class="caption">
				<center><?php
					if(!empty($data_produk['Nama_Hasil']))
					{
						echo $data_produk['Nama_Hasil'];
					}
					elseif(!empty($data_produk['Nama_Alat']))
					{
						echo $data_produk['Nama_Alat'];	
					}
					elseif(!empty($data_produk['Nama_Bahan']))
					{
						echo $data_produk['Nama_Bahan'];
					}
					?></center>
					<br>
					<br>
					<center><a href="index.php?halaman=isi_ulasan&id_produk=<?php echo $data['ID_Produk']; ?>"" class="btn btn-success">Detail Ulasan</a></center>
					<br>
					<br>
				</div>
			</div>
		</div>
	<?php endforeach ?>
</div>
