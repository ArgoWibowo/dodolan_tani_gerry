<?php 
$idpro = $_GET['id_produk'];
$user = $_SESSION['penjual'];
$id_user = $_SESSION['penjual']['ID_User'];


$dataulasan = $penjual->tampil_ulasan($idpro);

$dataproduk = $produk->tampil_penawaran_produk_user($id_user);

$dataproduk['Nama_Hasil']="";
$dataproduk['Nama_Alat']="";
$dataproduk['Nama_Bahan']="";
$dataproduk['Deskripsi_Hasil']='';
$dataproduk['Deskripsi_ALat']='';
$dataproduk['Deskripsi_Bahan']='';
?>
<?php if ($dataulasan==array()): ?>
	<div class="alert alert-danger">Tidak Ada Ulasan</div>
<?php else: ?>
	<div class="row">
		<?php $data = $produk->ambil_penawaran_produk($_GET['id_produk']); ?>
		<?php $data_produk = $produk->ambil_produk_penawaran($data['ID']); ?>
		<div class="col-md-4">
			<table class="table">
				<tr>
					<td><div class="thumbnail">
						<img src="../asset/img/produk/<?php echo $data['Gambar1']; ?>" class="img-responsive" width="200">
					</div>
				</td>
			</tr>
			<tr>
				<th>Nama</th>
				<td> <?php
					if(!empty($data_produk['Nama_Hasil']))
					{
						echo $data_produk['Nama_Hasil'];
					}
					elseif(!empty($data_produk['Nama_Alat']))
					{
						echo $data_produk['Nama_Alat'];	
					}
					elseif(!empty($data_produk['Nama_Bahan']))
					{
						echo $data_produk['Nama_Bahan'];
					}
					?> 
				</td>
			</tr>
			<tr>
				<th>Deskripsi</th>
				<td>
					<?php
					if(!empty($data_produk['Deskripsi_Hasil']))
					{
						echo $data_produk['Deskripsi_Hasil'];
					}
					elseif(!empty($data_produk['Deskripsi_Alat']))
					{
						echo $data_produk['Deskripsi_Alat'];	
					}
					elseif(!empty($data_produk['Deskripsi_Bahan']))
					{
						echo $data_produk['Deskripsi_Bahan'];
					}
					?>
				</td>
			</tr>
		</tr>
		<tr>
			<th>Spesifikasi</th>
			<td> <?php echo $data['Spesifikasi_Barang']; ?></td>
		</tr>
		<tr>
			<tr>
				<th>Harga</th>
				<td> <?php echo $data_produk['Harga']; ?></td>
			</tr>
			<th>Kondisi</th>
			<td> <?php echo $data['Kondisi_Barang']; ?></td>
		</tr>
		<tr>
			<th>Merk</th>
			<td> <?php echo $data['Merk']; ?></td>
		</tr>
		<tr>
			<th>Tahun Produksi</th>
			<td> <?php echo $data['Tahun_Produksi']; ?></td>
		</tr>
		<tr>
			<th>Satuan Produk</th>
			<td> <?php echo $data['Satuan_Barang']; ?></td>
		</tr>
	</table>
</div>
<div class="col-md-8">
	<?php foreach ($dataulasan as $key => $value): ?>
		<div class="panel panel-info">
			<div class="panel-heading">
				<h3 class="panel-title"><?php echo $value['nama'] ?></h3>
				<strong> <?php echo $value['Waktu']; ?></strong>
			</div>
			<div class="panel-body">
				<?php echo $value['Komentar']; ?>
			</div>
		</div>
	<?php endforeach ?>	 
</div>
</div>
<?php endif ?>
