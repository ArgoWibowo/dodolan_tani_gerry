<?php 

$id_bahan = $_GET['kode'];

$id = $_GET['id'];

$ambil = $produk->ambil_produk_bahan($id_bahan, $id);


?>
<h2>Ubah Produk</h2>
<form method="post" enctype="multipart/form-data">
	<br>
	<!-- <div class="form-group">
		<label>Kode Bahan</label>
		<input type="text"  class="form-control" name="id_bahan" value=" <?php echo $ambil['ID_Bahan']; ?>" readonly>
	</div> -->
	<div class="form-group">
		<label>Nama Bahan</label>
		<input type="text"  class="form-control" name="nama_bahan" value=" <?php echo $ambil['Nama_Bahan']; ?>">
	</div>
	<div class="form-group">
		<label>Deskripsi BAhan</label>
		<textarea class="form-control" name="deskripsi_bahan" required=""> <?php echo $ambil['Deskripsi_Bahan']; ?></textarea>
	</div>
	<div class="form-group">
		<label>Spesifikasi Bahan</label>
		<textarea class="form-control" name="spesifikasi_bahan" required=""> <?php echo $ambil['Spesifikasi_Bahan']; ?></textarea>
	</div>
	<div class="form-group">
		<label>Harga Terendah</label>
		<input type="number"  class="form-control" name="harga_terendah" value="<?php echo $ambil['Harga_Terendah']; ?>">
	</div>
	<div class="form-group">
		<label>Harga Tertinggi</label>
		<input type="number"  class="form-control" name="harga_tertinggi" value="<?php echo $ambil['Harga_Tertinggi']; ?>">
	</div>
	<div>
		<label>Harga Jual</label>
		<input type="number" class="form-control" name="Harga_Pas" required="" value="<?php echo $ambil['Harga']; ?>">
	</div>
	<br>
	<div class="form-group">
		<label>Fungsi</label>
		<textarea class="form-control" name="fungsi" required=""> <?php echo $ambil['Fungsi_Bahan']; ?></textarea>
	</div>
	<div class="form-group">
		<label>Jenis Bahan</label>
		<input type="text"  class="form-control" name="jenis_bahan" value=" <?php echo $ambil['Jenis_Bahan']; ?>">
	</div>
	<button class="btn btn-warning" name="simpan_bahan">Simpan</button>
</form>

<?php 
if(isset($_POST["simpan_bahan"]))
{
	$produk->ubah_produk_bahan($_POST['nama_bahan'], $_POST['deskripsi_bahan'],$_POST['spesifikasi_bahan'],$_POST['harga_terendah'],$_POST['harga_tertinggi'], $_POST['Harga_Pas'], $_POST['fungsi'],$_POST['jenis_bahan'], $id_bahan, $id);

	echo "<script>alert('Data berhasil diubah'); location='index.php?halaman=produk'</script>";
}

?>