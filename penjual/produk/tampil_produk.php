<?php 
$pengguna = $_SESSION['penjual']['ID_User'];

$semuahasil = $produk->tampil_hasil_produk($pengguna);

$semuaalat = $produk->tampil_produk_alat($pengguna);

$semuabahan = $produk->tampil_produk_bahan($pengguna);


// echo "<pre>";
// print_r($semuahasil);
// echo "</pre>";

?>


<h2>Daftar Produk</h2>
<a href="index.php?halaman=tambah_produk" class="btn btn-primary">Tambah Produk</a>
<br>
<br>
<ul class="nav nav-tabs">
	<li class="active"><a data-toggle="tab" href="#produk">Produk Hasil Tani</a></li>
	<li><a data-toggle="tab" href="#alat">Produk ALat Tani</a></li>
	<li><a data-toggle="tab" href="#bahan">Produk Bahan Tani</a></li>
	<!-- <li><a data-toggle="tab" href="#penawaran">Penawaran Produk</a></li> -->
</ul>

<div class="tab-content">
	<div id="produk" class="tab-pane fade in active">
		<h3>Produk Hasil Tani</h3>
		<div class="table-responsive">
			<table class="table table-bordered table-striped" id="thetable">
				<thead>
					<tr>
						<th>NO</th>
						<th>NAMA HASIL</th>
						<th>SPESIES</th>
						<th>DESKRIPSI HASIL</th>
						<th>HARGA TERENDAH</th>
						<th>HARGA TERTINGGI</th>
						<th>HARGA JUAL</th>
						<th>MASA EXP</th>
						<th>SATUAN</th>
						<th colspan="4" class="text-center">OPSI</th>
					</tr>
				</thead>
				<tbody>
					<?php foreach ($semuahasil as $key => $value): ?>
						<tr>
							<td> <?php echo $key+1; ?></td>
							<td> <?php echo $value['Nama_Hasil']; ?></td>
							<td> <?php echo $value['Nama_Tanaman']; ?></td>
							<td> <?php echo $value['Deskripsi_Hasil']; ?></td>
							<td> <?php echo $value['Harga_Terendah']; ?></td>
							<td> <?php echo $value['Harga_Tertinggi']; ?></td>
							<td> <?php echo $value['Harga'];?></td>
							<td> <?php echo $value['Masa_Expayet']; ?></td>
							<td> <?php echo $value['Satuan']; ?></td>
							<td> 
								<center><a href="index.php?halaman=ubah_produk&kode=<?php echo $value['ID_Hasil']; ?>&id=<?php echo $value['ID_Produk'];?>" class="btn btn-warning">Ubah</a></center>
							</td>
							<td>
								<center><a href="index.php?halaman=hapus_produk&kode=<?php echo $value['ID_Hasil']; ?>&id=<?php echo $value['ID_Produk'];?>" class="btn btn-danger">Hapus</a></center>
							</td>
							<?php  if ($data_penawaran = $produk->cek_penawaran($value['ID_Produk']));
							?>
							<td>
								<?php if (empty($data_penawaran)): ?>
								<?php else: ?>
									<?php if ($data_penawaran[0]['Status']=="Mati"): ?>
										<a href="Index.php?halaman=aktif&id=<?php echo $value['ID_Produk'];?>"	class="btn btn-primary">On</a>
									<?php elseif($data_penawaran[0]['Status']=="Aktif"): ?>
										<center><a href="index.php?halaman=tidak_aktif&kode=<?php echo $value['ID_Hasil']; ?>&id=<?php echo $value['ID_Produk'];?>" class="btn btn-danger">Off</a></center>
									<?php endif ?>
								<?php endif ?>
							</td>
							<td>
								<center>
									<?php if ($data_penawaran!==array()): ?>
										<?php if ($data_penawaran[0]['Status']=="Aktif"): ?>
											<span class="label label-success"><?php echo "Aktif"; ?></span>
										<?php elseif($data_penawaran[0]['Status']=="Mati"): ?>
											<span class="label label-danger">Non Aktif</span>
										<?php else: ?>
											<span class="label label-warning">Tunggu Konfirmasi</span>
										<?php endif ?>
									<?php else: ?>
										<a href="Index.php?halaman=tambah_penawaran&kode=<?php echo $value['ID_Hasil']; ?>"	class="btn btn-primary">Tawarakan</a>
									<?php endif ?>
								</center>
							</td>
						</tr>
					<?php endforeach ?>
				</center>
			</tbody>
		</table>
	</div>
</div>
<div id="alat" class="tab-pane fade">
	<div class="table-responsive">
		<h3>Produk Alat Tani</h3>
		<table class="table table-bordered table-striped" id="thetable2">
			<thead>
				<tr>
					<th>NO</th>
					<th>NAMA ALAT</th>
					<th>DESKRIPSI ALAT</th>
					<th>SPESIFIKASI</th>
					<th>HARGA TERENDAH</th>
					<th>HARGA TERTINGGI</th>
					<th>HARGA JUAL</th>
					<th>FUNGSI</th>
					<th colspan="4" class="text-center">OPSI</th>
				</tr>
			</thead>
			<tbody>
				<?php foreach ($semuaalat as $key => $value): ?>
					<tr>
						<td> <?php echo $key+1; ?></td>
						<td> <?php echo $value['Nama_Alat'] ;?></td>
						<td> <?php echo $value['Deskripsi_Alat']; ?></td>
						<td> <?php echo $value['Spesifikasi']; ?></td>
						<td> <?php echo $value['Harga_Terendah']; ?></td>
						<td> <?php echo $value['Harga_Tertinggi'] ;?></td>
						<td> <?php echo $value['Harga'];?></td>
						<td> <?php echo $value['Fungsi']; ?></td>
						<td> 
							<center><a href="index.php?halaman=ubah_produk_alat&kode=<?php echo $value['ID_Alat']; ?>&id=<?php echo $value['ID_Produk'];?>" class="btn btn-warning">Ubah</a></center>
						</td>
						<td>
							<center><a href="index.php?halaman=hapus_produk_alat&kode=<?php echo $value['ID_Alat']; ?>&id=<?php echo $value['ID_Produk'];?>" class="btn btn-danger">Hapus</a></center>
						</td>
						<?php  if ($data_penawaran = $produk->cek_penawaran($value['ID_Produk']));
						?>
						<td>
							<?php if (empty($data_penawaran)): ?>
							<?php else: ?>
								<?php if ($data_penawaran[0]['Status']=="Mati"): ?>
									<a href="Index.php?halaman=aktif&id=<?php echo $value['ID_Produk'];?>"	class="btn btn-primary">On</a>
								<?php elseif($data_penawaran[0]['Status']=="Aktif"): ?>
									<center><a href="index.php?halaman=tidak_aktif&kode=<?php echo $value['ID_Alat']; ?>&id=<?php echo $value['ID_Produk'];?>" class="btn btn-danger">Off</a></center>
								<?php endif ?>
							<?php endif ?>
						</td>
						<td>
							<center>
								<?php if ($data_penawaran!==array()): ?>
									<?php if ($data_penawaran[0]['Status']=="Aktif"): ?>
										<span class="label label-success"><?php echo "Aktif"; ?></span>
									<?php elseif($data_penawaran[0]['Status']=="Mati"): ?>
										<span class="label label-danger">Non Aktif</span>
									<?php else: ?>
										<span class="label label-warning">Tunggu Konfirmasi</span>
									<?php endif ?>
								<?php else: ?>
									<a href="Index.php?halaman=tambah_penawaran&kode=<?php echo $value['ID_Alat']; ?>"	class="btn btn-primary">Tawarakan</a>
								<?php endif ?>
							</center>
						</td>
					</tr>
				<?php endforeach ?>
			</center>
		</tbody>
	</table>
</div>
</div>
<div id="bahan" class="tab-pane fade">
	<div class="table-responsive">
		<h3>Produk Bahan Tani</h3>
		<table class="table table-bordered table-striped" id="thetable3">
			<thead>
				<tr>
					<th>NO</th>
					<th>NAMA BAHAN</th>
					<th>DESKRIPSI BAHAN</th>
					<th>SPESIFIKASI BAHAN</th>
					<th>HARGA TERENDAH</th>
					<th>HARGA TERTINGGI</th>
					<th>HARGA JUAL</th>
					<th>FUNGSI BAHAN</th>
					<th>JENIS BAHAN</th>
					<th colspan="4" class="text-center">OPSI</th>
				</tr>
			</thead>
			<tbody>
				<?php foreach ($semuabahan as $key => $value): ?>
					<tr>
						<td> <?php echo $key+1; ?></td>
						<td> <?php echo $value['Nama_Bahan']; ?></td>
						<td> <?php echo $value['Deskripsi_Bahan']; ?></td>
						<td> <?php echo $value['Spesifikasi_Bahan']; ?></td>
						<td> <?php echo $value['Harga_Terendah']; ?></td>
						<td> <?php echo $value['Harga_Tertinggi']; ?></td>
						<td> <?php echo $value['Harga'];?></td>
						<td> <?php echo $value['Fungsi_Bahan']; ?></td>
						<td> <?php echo $value['Jenis_Bahan']; ?></td>
						<td> 
							<center><a href="index.php?halaman=ubah_produk_bahan&kode=<?php echo $value['ID_Bahan']; ?>&id=<?php echo $value['ID_Produk'];?>" class="btn btn-warning">Ubah</a></center>
						</td>
						<td>
							<center><a href="index.php?halaman=hapus_produk_bahan&kode=<?php echo $value['ID_Bahan']; ?>&id=<?php echo $value['ID_Produk'];?>" class="btn btn-danger">Hapus</a></center>
						</td>
						<?php  if ($data_penawaran = $produk->cek_penawaran($value['ID_Produk']));
						?>
						<td>
							<?php if (empty($data_penawaran)): ?>
							<?php else: ?>
								<?php if ($data_penawaran[0]['Status']=="Mati"): ?>
									<a href="Index.php?halaman=aktif&id=<?php echo $value['ID_Produk'];?>"	class="btn btn-primary">On</a>
								<?php elseif($data_penawaran[0]['Status']=="Aktif"): ?>
									<center><a href="index.php?halaman=tidak_aktif&kode=<?php echo $value['ID_Bahan']; ?>&id=<?php echo $value['ID_Produk'];?>" class="btn btn-danger">Off</a></center>
								<?php endif ?>
							<?php endif ?>
						</td>
						<td>
							<center>
								<?php if ($data_penawaran!==array()): ?>
									<?php if ($data_penawaran[0]['Status']=="Aktif"): ?>
										<span class="label label-success"><?php echo "Aktif"; ?></span>
									<?php elseif($data_penawaran[0]['Status']=="Mati"): ?>
										<span class="label label-danger">Non Aktif</span>
									<?php else: ?>
										<span class="label label-warning">Tunggu Konfirmasi</span>
									<?php endif ?>
								<?php else: ?>
									<a href="Index.php?halaman=tambah_penawaran&kode=<?php echo $value['ID_Bahan']; ?>"	class="btn btn-primary">Tawarakan</a>
								<?php endif ?>
							</center>
						</td>
					</tr>
				<?php endforeach ?>
			</center>
		</tbody>
	</table>
</div>
</div>
</div>