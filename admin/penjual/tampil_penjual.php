<?php 

$data_penjual = $penjual->tampil_penjual();

?>

<h2>Data Penjual</h2>
<table class="table table-bordered table-striped" id="thetable">
	<thead>
		<tr>
			<th>NO</th>
			<th>KODE PENJUAL</th>
			<th>NAMA PENJUAL</th>
			<th>ALAMAT</th>
			<th>NO TELEPON</th>
			<th>EMAIL</th>
			<th><center>OPTION</center></th>
			<th class="text-center">AKSI</th>
		</tr>
	</thead>
	<tbody>
		<?php foreach ($data_penjual as $key => $value): ?>
			<tr>
				<td> <?php echo $key+1; ?></td>
				<td> <?php echo $value['ID_User'] ?></td>
				<td> <?php echo $value['nama']; ?></td>
				<td> <?php echo $value['alamat']; ?></td>
				<td> <?php echo $value['nomor_telpon']; ?></td>
				<td> <?php echo $value['Email']; ?></td>
				<td>
					<center><a href="index.php?halaman=detail_user&kode=<?php echo $value['ID_User']; ?>"" class="btn btn-primary">Detail</a></center>
				</td>
				<td>
					<center><a href="index.php?halaman=produk_penjual&kode=<?php echo $value['ID_User']; ?>" class="btn btn-success">Produk</a></center>
				</td>
			</tr>
		<?php endforeach ?>
	</tbody>
</table>