<?php 

$id_user = $_GET['kode'];
$datauser = $user->ambil_detail_user($id_user);

?>
<div class="row">
	<div class="col-md-4">
		<div align="center"><img src="../asset/img/foto/<?php echo $datauser['Foto']; ?>" class="img-square img-responsive" width="200" height="200"></div>
		<br>		
		<table class="table">
			<tr>
				<th>Nama Lengkap</th>
				<td> <?php echo $datauser['nama']; ?></td>
			</tr>
			<tr>
				<th>Jenis Kelamin</th>
				<td> <?php echo $datauser['jenis_kelamin']; ?></td>
			</tr>
			<tr>
				<th>Tanggal Lahir</th>
				<td> <?php echo $datauser['tanggal_lahir']; ?></td>
			</tr>
			<tr>
				<th>Alamat</th>
				<td> <?php echo $datauser['alamat']; ?></td>
			</tr>
			<tr>
				<th>Provinsi</th>
				<td> <?php echo $datauser['provinsi']; ?></td>
			</tr>
			<tr>
				<th>Kabupaten</th>
				<td> <?php echo $datauser['kabupaten']; ?></td>
			</tr>
			<tr>
				<th>Kecamatan</th>
				<td> <?php echo $datauser['kecamatan']; ?></td>
			</tr>
			<tr>
				<th>Kelurahan</th>
				<td> <?php echo $datauser['kelurahan_desa']; ?></td>
			</tr>
			<tr>
				<th>Nomor Telepon</th>
				<td> <?php echo $datauser['nomor_telpon']; ?></td>
			</tr>
			<tr>
				<th>Email</th>
				<td> <?php echo $datauser['Email']; ?></td>
			</tr>
		</table>
		<td> 
			<a href="index.php?halaman=ubah_detail_user&kode=<?php echo $datauser['ID_User']; ?>" class="btn btn-warning">Ubah</a></td>     
			<td>
			<a href="index.php?halaman=hapus_detail_user&kode=<?php echo $datauser['ID_User']; ?>" class="btn btn-danger">Hapus</a
			</td>
		</div>
	</div>