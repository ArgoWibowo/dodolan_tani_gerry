<?php 

$id_user = $_GET['kode'];

$datauser = $user->ambil_detail_user($id_user);


?>

<h2>Detail User <?php echo $datauser['nama']; ?></h2>
<form method="post" enctype="multipart/form-data">
	<br>
	<div>
		<label>Nama Lengkap</label>
		<input type="text" class="form-control" name="nama" required="" value="<?php echo $datauser['nama']; ?>">
	</div>
	<br>
	<div>
		<label>Jenis Kelamin</label>
		<input type="text" class="form-control" name="jns_kelamin" required="" value="<?php echo $datauser['jenis_kelamin']; ?>">
	</div>
	<br>

	<div>
		<label>Tanggal Lahir</label>
		<input type="date" class="form-control" name="tgl_lhr" required="" value="<?php echo $datauser['tanggal_lahir']; ?>">
	</div>
	<br>

	<div>
		<label>Alamat</label>
		<input type="text" class="form-control" name="alamat" required="" value="<?php echo $datauser['alamat']; ?>">
	</div>
	<br>

	<div>
		<label>Provinsi</label>
		<input type="text" class="form-control" name="provinsi" required="" value="<?php echo $datauser['provinsi']; ?>">
	</div>
	<br>

	<div>
		<label>Kabupaten</label>
		<input type="text" class="form-control" name="kabupaten" required="" value="<?php echo $datauser['kabupaten']; ?>">
	</div>
	<br>
	<div>
		<label>Kecamatan</label>
		<input type="text" class="form-control" name="kecamatan" required="" value="<?php echo $datauser['kecamatan']; ?>">
	</div>
	<br>
	<div>
		<label>Kelurahan</label>
		<input type="text" class="form-control" name="kelurahan" required="" value="<?php echo $datauser['kelurahan_desa']; ?>">
	</div>
	<br>
	<div>
		<label>Nomor Telpon</label>
		<input type="text" class="form-control" name="notlpn" required="" value="<?php echo $datauser['nomor_telpon']; ?>">
	</div>
	<br>
	<div>
		<label>Email</label>
		<input type="text" class="form-control" name="email" required="" value="<?php echo $datauser['Email']; ?>">
	</div>
	<br>
	<button class="btn btn-warning" name="simpan">Simpan</button>
</form>

<?php 

if(isset($_POST["simpan"]))
{
	$user->ubah_detail_user($_POST['nama'], $_POST['jns_kelamin'], $_POST['tgl_lhr'], $_POST['alamat'], $_POST['provinsi'], $_POST['kabupaten'], $_POST['kecamatan'], $_POST['kelurahan'], $_POST['notlpn'],  $_POST['email'], $id_user );

	echo "<script>alert('Data berhasil diubah'); location='index.php?halaman=penjual'</script>";
}

?>