<?php 
include  "../config/class.php";
$data_penawaran = $admin->tampil_penawaran_produk_tidak_aktif();
$data_penjual = $admin->tampil_penjual_tidak_aktif();

?>

<?php 
if(!isset($_SESSION['admin']))
{
  echo "<script>alert('Anda harus login'); </script>";
  echo "<script>location='login.php'; </script>";
}

?>

<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title >Admin</title>
  <link href="../asset/css/bootstrap.min.css" rel="stylesheet">
  <link rel="stylesheet" href="../asset/css/style.css">
  <link rel="stylesheet" href="../asset/font-awesome/css/font-awesome.css">
  <link rel="stylesheet" href="https://cdn.datatables.net/1.10.12/css/dataTables.bootstrap.min.css">
</head>
<body>

  <div id="wrapper">
    <nav class="navbar navbar-default hidden-print">
      <div class="navbar-header">
        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target=".sidebar-collapse" aria-expanded="false">
          <span class="sr-only">Toggle navigation</span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
        </button>
        <a class="navbar-brand" href="index.php">DODOLANTANI</a>
      </div>
    </nav>

    <nav class="navbar-default navbar-side hidden-print">
      <div class="sidebar-collapse">
        <ul class="nav" id="main-menu">
          <li><a href="index.php"><i class="fa fa-home"></i> Home</a></li>
          <li><a href="index.php?halaman=kategori"><i class="fa fa-tags"></i> Kategori Produk</a></li>
          <li><a href="index.php?halaman=produk"><i class="fa fa-product-hunt"></i> Daftar Produk</a></li>
          <li><a href="index.php?halaman=penawaran"><i class="fa fa-product-hunt"></i> Daftar Penawaran Produk <i class="badge"><?php echo count($data_penawaran); ?></i> </a></li>
          <li><a href="index.php?halaman=daftar_penjual"><i class="fa fa-user"></i> Daftar Penjual <i class="badge"><?php echo count($data_penjual); ?></i> </a></li>
          <li><a href="index.php?halaman=penjual"><i class="fa fa-user"></i> Data Penjual</a></li>
          <li><a href="index.php?halaman=transaksi"><i class="fa fa-user"></i> Data Transaksi</a></li>
          <li><a href="index.php?halaman=laporan"><i class="fa fa-table"></i> Laporan</a></li>
          <li><a href="index.php?halaman=logout"><i class="fa fa-sign-out"></i> Log Out</a></li>
        </ul>
      </div>
    </nav>


    <div id="page-wrapper">
      <div id="page-inner">
        <?php 
        //jika tidak ada parameter halaman maka
        if(!isset($_GET['halaman']))
        {
          include 'home.php';
        }
        else
        {
          if($_GET['halaman']=="kategori")
          {
            include 'kategori/tampil_kategori.php';
          }
          elseif($_GET['halaman']=="ubah_kategori")
          {
            include 'kategori/ubah_kategori.php';
          }
          elseif($_GET['halaman']=="hapus_kategori")
          {
            include 'kategori/hapus_kategori.php';
          }
          elseif($_GET['halaman']=="tambah_kategori")
          {
            include 'kategori/tambah_kategori.php';
          }
          elseif($_GET['halaman']=="produk")
          {
            include 'produk/tampil_produk.php';
          }
          elseif($_GET['halaman']=="ubah_produk")
          {
            include 'produk/ubah_produk.php';
          }
          elseif($_GET['halaman']=="ubah_produk_alat")
          {
            include 'produk/ubah_produk_alat.php';
          }
          elseif($_GET['halaman']=="ubah_produk_bahan")
          {
            include 'produk/ubah_produk_bahan.php';
          }
          elseif($_GET['halaman']=="hapus_produk")
          {
            include 'produk/hapus_produk.php';
          }
          elseif($_GET['halaman']=="hapus_produk_alat")
          {
            include 'produk/hapus_produk_alat.php';
          }
          elseif($_GET['halaman']=="hapus_produk_bahan")
          {
            include 'produk/hapus_produk_bahan.php';
          }
          elseif($_GET['halaman']=="tambah_produk")
          {
            include 'produk/tambah_produk.php';
          }
          elseif($_GET['halaman']=="penawaran")
          {
            include 'produk/tampil_penawaran.php';
          }
          elseif($_GET['halaman']=="penjual")
          {
            include 'penjual/tampil_penjual.php';
          }
          elseif($_GET['halaman']=="produk_penjual")
          {
            include 'penjual/tampil_produk.php';
          }
          elseif($_GET['halaman']=="daftar_penjual")
          {
            include 'penjual/daftar_penjual.php';
          }
          elseif($_GET['halaman']=="status")
          {
            include 'penjual/status_produk.php';
          }

          elseif($_GET['halaman']=="status_registrasi")
          {
            include 'penjual/status_registrasi.php';
          }
          elseif($_GET['halaman']=="transaksi")
          {
            include 'permintaan/tampil_transaksi.php';
          }
          elseif($_GET['halaman']=="pembayaran")
          {
            include 'permintaan/tampil_pembayaran.php';
          }
          elseif($_GET['halaman']=="status_permintaan")
          {
            include 'permintaan/status_permintaan.php';
          }
          elseif ($_GET['halaman']=="logout") 
          {
            include 'logout.php';
          }
          elseif ($_GET['halaman']=='status_produk_penjual')
          {
            include 'produk/status_produk_penjual.php';
          }
          elseif ($_GET['halaman']=='detail_user')
          {
            include 'penjual/detail_user.php';
          }
          elseif ($_GET['halaman']=='hapus_detail_user')
          {
            include 'penjual/hapus_detail_user.php';
          }
          elseif ($_GET['halaman']=='ubah_detail_user')
          {
            include 'penjual/ubah_detail_user.php';
          }
          elseif ($_GET['halaman']=='laporan')
          {
            include 'laporan/laporan_transaksi_penjual.php';
          }
          elseif ($_GET['halaman']=='kirim_bukti')
          {
            include 'permintaan/kirim_bukti.php';
          }
          elseif ($_GET['halaman']=='daftar_bukti')
           {
            include 'permintaan/daftar_bukti.php';
          }
        }

        ?>
        <br>
        <br>

      </div>
    </div>

    
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.12/js/dataTables.bootstrap.min.js"></script>
    <script src="../asset/js/bootstrap.min.js"></script>
    <script src="../asset/js/mobile.js"></script>
    <script src="../asset/js/table.js"></script>

  </body>
  </html>