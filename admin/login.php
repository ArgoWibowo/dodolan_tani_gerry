<?php 

include '../config/class.php';

?>

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Login</title>
</head>
<link rel="stylesheet" href="../asset/font-awesome/css/font-awesome.min.css">
<link rel="stylesheet" href="../asset/css/bootstrap.min.css">
<link rel="stylesheet" href="../asset/css/login.css">
<body>
	<div class="container">
		<div class="row">
			<div class="col-md-4 col-md-offset-4">
				<div class="kotak-login">
					<form method="post">
						<legend class="text-center"><i class="fa fa-lock"></i> Login Administrator</legend>
						<div class="form-group">
							<label>Email</label>
							<input type="email" class="form-control" name="email">
						</div>
						<div class="form-group">
							<label>Password</label>
							<input type="password" class="form-control" name="pass">
						</div>
						<center><button class="btn btn-primary" name="login">Login</button></center>
						<br>
					</form>
					<?php 
					if (isset($_POST['login']))
					{
						$hasil = $admin->login_admin($_POST['email'], $_POST['pass']);
						if($hasil=="sukses")
						{
							echo "<div class='alert alert-info'>Login sukses</div>";
							echo "<meta http-equiv='refresh' content='1;url=index.php'>";
						}
						else
						{
							echo "<div class='alert alert-danger'>Login gagal</div>";
							echo "<meta http-equiv='refresh' content='1;url=login.php'>";
						}
					}
					?>

					<br>
				</div>
			</div>
		</div>
	</div>

	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
	<script src="https://cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js"></script>
	<script src="https://cdn.datatables.net/1.10.12/js/dataTables.bootstrap.min.js"></script>
	<script src="../asset/js/bootstrap.min.js"></script>
	<script src="../asset/js/mobile.js"></script>
	<script src="../asset/js/table.js"></script>
	<script src="../asset/ckeditor/ckeditor.js"></script>
	<script>
		CKEDITOR.replace("theeditor");
	</script>
</body>
</html>