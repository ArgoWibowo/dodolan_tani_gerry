<?php 

$data_kategori = $kategori->tampil_kategori();

?>

<h2>Tambah Kategori</h2>
<form method="post" enctype="multipart/form-data">
	<br>
	<div>
		<label>Kode Kategori</label>
		<input type="text" class="form-control" name="id_kategori" required="">
	</div>
	<br>
	<div>
		<label>Jenis Kategori</label>
		<input type="text" class="form-control" name="jenis_kategori" required="">
	</div>
	<br>
	<div>
		<label>Nama Kategori</label>
		<input type="text" class="form-control" name="nama_kategori" required="">
	</div>
	<br>
	<button class="btn btn-warning" name="simpan">Simpan</button>
</form>

<?php 

if(isset($_POST['simpan']))
{
	$cek = $kategori->simpan_kategori($_POST["id_kategori"], $_POST["jenis_kategori"], $_POST["nama_kategori"]);

	if($cek=='Berhasil')
	{

		echo "<script>alert('Sukses,kategori tersimpan');</script>";
		echo "<script>location='index.php?halaman=kategori';</script>";
	}
	else
	{
		echo "<script>alert('Kode sudah terdaftar');</script>";
		echo "<script>location='index.php?halaman=tambah_kategori';</script>";
	}
}


?>