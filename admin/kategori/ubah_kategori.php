<?php 

$id_kat= $_GET['kode'];

$ambilkat= $kategori->ambil_kategori($id_kat);

?>

<h2>Ubah Kategori</h2>
<form method="post" enctype="multipart/form-data">
	<br>
	<div>
		<label>Kode Kategori</label>
		<input type="text" class="form-control" name="id_kategori" required="" value=" <?php echo $ambilkat['ID_Kategori']; ?>" readonly>
	</div>
	<br>
	<div>
		<label>Jenis Kategori</label>
		<input type="text" class="form-control" name="jenis_kategori" required="" value=" <?php echo $ambilkat['Jenis_kategori']; ?>">
	</div>
	<br>
	<div>
		<label>Nama Kategori</label>
		<input type="text" class="form-control" name="nama_kategori" required="" value=" <?php echo $ambilkat['Nama_kategori']; ?>">
	</div>
	<br>
	<button class="btn btn-warning" name="simpan">Simpan</button>
</form>

<?php 

if(isset($_POST['simpan']))
{
	$kategori->ubah_kategori($_POST['jenis_kategori'], $_POST['nama_kategori'], $id_kat);

	echo "<script>alert('Data berhasil diubah'); location='index.php?halaman=kategori'</script>";
}


?>