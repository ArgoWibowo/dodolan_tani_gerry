<?php 

$semuakategori = $kategori->tampil_kategori();

// echo "<pre>";
// print_r($semuakategori);
// echo "</pre>";

?>

<h2>Daftar Kategori Produk</h2>
<a href="index.php?halaman=tambah_kategori" class="btn btn-primary">Tambah Kategori</a>
<br>
<br>
<table class="table table-bordered table-striped" id="thetable">
	<thead>
		<tr>
			<th>NO</th>
			<th>KODE KATEGORI</th>
			<th>JENIS KATEGORI</th>
			<th>NAMA KATEGORI</th>
			<th class="text-center">AKSI</th>
			<th class="text-center">AKSI</th>
		</tr>
	</thead>
	<tbody>
		<?php foreach ($semuakategori as $key => $value): ?>
			<tr>
				<td> <?php echo $key+1; ?></td>
				<td> <?php echo $value['ID_Kategori']; ?></td>
				<td> <?php echo $value['Jenis_kategori']; ?></td>
				<td> <?php echo $value['Nama_kategori']; ?></td>
				<td>  
					<center><a href="index.php?halaman=ubah_kategori&kode=<?php echo $value['ID_Kategori']; ?>" class="btn btn-warning">Ubah</a> </center>
				</td>  
				<td> 
					<center><a href="index.php?halaman=hapus_kategori&kode=<?php echo $value['ID_Kategori'];?>" class="btn btn-danger">Hapus</a> </center>

				</td>  

			</tr>
		<?php endforeach ?>
	</tbody>
</table>

