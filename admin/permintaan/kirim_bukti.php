<?php 
$id_permintaan = $_GET['kode'];

?>

<h3>Kirim Bukti Transfer Ke Penjual</h3>
<form method="post" enctype="multipart/form-data">
	<div class="form-group">
		<label>Kode Permintaan</label>
		<input type="text" name="kd_permintaan" class="form-control" value="<?php echo $id_permintaan; ?>" readonly >
	</div>
	<div class="form-group">
		<label>Nama Rekening</label>
		<input type="text" name="nama_rek" class="form-control">
	</div>
	<div class="form-group">
		<label>No Rekening</label>
		<input type="text" name="no_rekening" class="form-control">
	</div>
	<div class="form-group">
		<label>Jumlah Transfer</label>
		<input type="text" name="jml_transfer" class="form-control">
	</div>
	<div class="form-group">
		<label>Bukti Transfer</label>
		<input type="file" name="bukti" class="form-control">
	</div>
	<div class="form-group">
		<label>Keterangan</label>
		<textarea class="form-control" name="keterangan"> </textarea>
	</div>
	<button class="btn btn-primary" name="kirim">Kirim</button>
</form>

<?php 

if(isset($_POST["kirim"]))
{
	$admin->kirim_bukti_penjual($_POST["nama_rek"], $_POST["no_rekening"], $_POST["jml_transfer"], $_FILES["bukti"], $_POST["keterangan"], $id_permintaan, $_GET['code']);

	echo "<div class='alert alert-info'>Berhasil Konfirmasi</div>";
	echo "<meta http-equiv='refresh' content='1;url='>";

}

?>