<?php 

$data_trans = $pembeli->tampil_semua_transaksi()

?>

<!-- <pre>
	<?php print_r($data_trans); ?>
</pre>  -->


<h2>Daftar Transaksi Penjualan</h2>
<br>
<div class="table-responsive">
	<table class="table table-bordered table-striped" >
		<thead>
			<tr>
				<th>NO</th>
				<th>KODE PERMINTAAN</th>
				<th>KODE USER</th>
				<th>KODE PENAWARAN</th>
				<th>QTY</th>
				<th>HARGA</th>
				<th>TANGGAL KEBUTUHAN</th>
				<th>TANGGAL PERMINTAAN</th>
				<th>STATUS</th>
				<th>STATUS KIRIM</th>
				<th><center>AKSI</center></th>
			</tr>
		</thead>
		<tbody>
			<?php foreach ($data_trans as $key => $value): ?>
				<tr>
					<td> <?php echo $key+1; ?></td>
					<td> <?php echo $value['ID_Permintaan'];?> </td>
					<td> <?php echo $value['ID_User'];?> </td>
					<td> <?php echo $value['ID_Penawaran'];?> </td>
					<td> <?php echo $value['Qty'];?> </td>
					<td> <?php echo $value['Harga'];?> </td>
					<td> <?php echo $value['Tgl_Kebutuhan'];?> </td>
					<td> <?php echo $value['Tgl_Permintaan'];?> </td>
					<td> <?php echo $value['Status'];?> </td>
					<td>
						<?php if ($value['Status_Kirim']=="Sudah Kirim"): ?>
							<label class="label label-success"><?php echo $value['Status_Kirim']; ?></label>
						<?php endif ?>
					</td>
					<td>
						<center><a href="index.php?halaman=pembayaran&kode=<?php echo $value['ID_Permintaan']; ?>" class="btn btn-warning btn-block">Cek Pembayaran</a></center>
						<br>
						<center><a href="index.php?halaman=daftar_bukti&kode=<?php echo $value['ID_Permintaan']; ?>" class="btn btn-success btn-block">Bukti Penjual</a></center>
					</td>
				</tr>
			<?php endforeach ?>
		</tbody>
	</table>
</div>