<?php

$data_spesies = $spesies->tampil_spesies(); 

?>  

<?php $data_kategori = $kategori->tampil_kategori() ;?>

<?php
// jika ada select di dalam form dengan name kategori
if(isset($_POST['kategori']))
{
	// select dengan name kategori disimpan di $kategori_produk
	$kategori_produk = $_POST['kategori'];
}
else
{
	$kategori_produk='';
}

// mengambil data kategori 
$ambil = $kategori->ambil_kategori($kategori_produk);
?>
<h2>Tambah Produk</h2>
<form method="post" enctype="multipart/form-data">
	<div class="form-group">
		<label>Kategori Produk</label>
		<select name="kategori" class="form-control" onchange="submit()">
			<option>-- Pilih Kategori --</option>
			<?php foreach ($data_kategori as $key => $value): ?>
				<option value="<?php echo $value['ID_Kategori'] ?>" <?php if($kategori_produk==$value['ID_Kategori']){echo "selected";} ?>><?php echo $value['Nama_kategori']; ?></option>
			<?php endforeach ?>
		</select>
	</div>
	<?php if ($ambil['Nama_kategori']=="Alat"): ?>
		<div class="form-group">
			<label>Kode Alat</label>
			<input type="text"  class="form-control" name="id_alat"></ins>
		</div>
		<div class="form-group">
			<label>Nama Alat</label>
			<input type="text"  class="form-control" name="nama_alat"></ins>
		</div>
		<div class="form-group">
			<label>Deskripsi Alat</label>
			<textarea class="form-control" name="deskripsi_alat" required=""></textarea>
		</div>
		<div class="form-group">
			<label>Spesifikasi</label>
			<textarea class="form-control" name="spesifikasi" required=""></textarea>
		</div>
		<div class="form-group">
			<label>Harga Terendah</label>
			<input type="text"  class="form-control" name="harga_terendah"></ins>
		</div>
		<div class="form-group">
			<label>Harga Tertinggi</label>
			<input type="text"  class="form-control" name="harga_tertinggi"></ins>
		</div>
		<div class="form-group">
			<label>Fungsi</label>
			<textarea class="form-control" name="fungsi" required=""></textarea>
		</div>
		<button class="btn btn-warning" name="simpan_alat">Simpan</button>
	<?php endif ?>

	<?php if ($ambil['Nama_kategori']=="Bahan"): ?>
		<div class="form-group">
			<label>Kode Bahan</label>
			<input type="text"  class="form-control" name="id_bahan"></ins>
		</div>
		<div class="form-group">
			<label>Nama Bahan</label>
			<input type="text"  class="form-control" name="nama_bahan"></ins>
		</div>
		<div class="form-group">
			<label>Deskripsi Bahan</label>
			<textarea class="form-control" name="deskripsi_bahan" required=""></textarea>
		</div>
		<div class="form-group">
			<label>Spesifikasi Bahan</label>
			<textarea class="form-control" name="spesifikasi_bahan" required=""></textarea>
		</div>
		<div class="form-group">
			<label>Harga Terendah</label>
			<input type="text"  class="form-control" name="harga_terendah"></ins>
		</div>
		<div class="form-group">
			<label>Harga Tertinggi</label>
			<input type="text"  class="form-control" name="harga_tertinggi"></ins>
		</div>
		<div class="form-group">
			<label>Fungsi Bahan</label>
			<textarea class="form-control" name="fungsi_bahan" required=""></textarea>
		</div>
		<div class="form-group">
			<label>Jenis Bahan</label>
			<input type="text"  class="form-control" name="jenis_bahan"></ins>
		</div>
		<button class="btn btn-warning" name="simpan_bahan">Simpan</button>
	<?php endif ?>

	<?php if ($ambil['Nama_kategori']=="Hasil"): ?>
		<div class="form-group">
			<label>Kode Hasil</label>
			<input type="text" class="form-control" name="ID_Hasil" required="">
		</div>
		<div class="form-group">
			<label>Nama Hasil</label>
			<input type="text" class="form-control" name="Nama_Hasil" required="">
		</div>

		<div class="form-group">
			<label>Spesies</label>
			<select class="form-control" name="ID_Spesies">
				<option value="">--Pilih Spesies--</option>
				<?php foreach ($data_spesies as $X => $Y): ?>
					<option value="<?php echo $Y['ID_Spesies'] ?>"><?php echo $Y['Nama_Tanaman']; ?></option>
				<?php endforeach ?>
			</select>
		</div>
		<div class="form-group">
			<label>Deskripsi Hasil</label>
			<textarea class="form-control" name="Deskripsi_Hasil" required=""></textarea>
		</div>
		<div class="form-group">
			<label>Harga Terendah</label>
			<input type="number" class="form-control" name="Harga_Terendah" required="">
		</div>
		<div class="form-group">
			<label>Harga Tertinggi</label>
			<input type="number" class="form-control" name="Harga_Tertinggi" required="">
		</div>
		<div class="form-group">
			<label>Satuan</label>
			<input type="text" class="form-control" name="Satuan" required="">
		</div>
		<button class="btn btn-warning" name="simpan">Simpan</button>
	<?php endif ?>
</form>
<?php 
if(isset($_POST["simpan"]))
{
	$cek = $produk->simpan_hasil_produk($_POST["ID_Hasil"], $_POST["Nama_Hasil"], $_POST["ID_Spesies"], $_POST["Deskripsi_Hasil"], $_POST["Harga_Terendah"], $_POST["Harga_Tertinggi"], $_POST["Satuan"], $kategori_produk);

	// echo $cek;
	//jika isi dari $cek sama dengan berhasil maka
	if($cek=='berhasil')
	{

		echo "<script>alert('Sukses,data produk tersimpan');</script>";
		echo "<script>location='index.php?halaman=produk';</script>";
	}
	else
	{
		echo "<script>alert('Kode sudah terdaftar');</script>";
		echo "<script>location='index.php?halaman=tambah_produk';</script>";
	}
}
elseif(isset($_POST["simpan_alat"]))
{
	$cek = $produk->simpan_produk_alat($_POST["id_alat"], $_POST["nama_alat"], $_POST["deskripsi_alat"], $_POST["spesifikasi"], $_POST["harga_terendah"], $_POST["harga_tertinggi"], $_POST["fungsi"], $kategori_produk );
	if($cek=='berhasil')
	{

		echo "<script>alert('Sukses,data produk alat tersimpan');</script>";
		echo "<script>location='index.php?halaman=produk#alat';</script>";
	}
	else
	{
		echo "<script>alert('Kode sudah terdaftar');</script>";
		echo "<script>location='index.php?halaman=tambah_produk';</script>";
	}
}
elseif(isset($_POST["simpan_bahan"]))
{
	$cek = $produk->simpan_produk_bahan($_POST["id_bahan"], $_POST["nama_bahan"], $_POST["deskripsi_bahan"], $_POST["spesifikasi_bahan"], $_POST["harga_terendah"], $_POST["harga_tertinggi"], $_POST["fungsi_bahan"], $_POST["jenis_bahan"], $kategori_produk );

	if($cek=='berhasil')
	{

		echo "<script>alert('Sukses,data produk bahan tersimpan');</script>";
		echo "<script>location='index.php?halaman=produk#bahan';</script>";
	}
	else
	{
		echo "<script>alert('Kode sudah terdaftar');</script>";
		echo "<script>location='index.php?halaman=tambah_produk';</script>";
	}
}

?>