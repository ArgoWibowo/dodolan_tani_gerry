`<?php include "config/class.php" ?>

<!DOCTYPE html>
<html>
<head>
	<title>Dodolantani</title>
	<link rel="stylesheet" type="text/css" href="asset/css/bootstrap.css">
	<link rel="stylesheet" href="asset/font-awesome/css/font-awesome.css">
	<link rel="stylesheet" type="text/css" href="asset/css/styleindex.css">
  <link rel="stylesheet" type="text/css" href="asset/OwlCarousel/dist/assets/owl.carousel.min.css">
  <link rel="stylesheet" type="text/css" href="asset/OwlCarousel/dist/assets/owl.theme.default.min.css">
</head>
<body>
	<?php include 'menu.php' ?>
  <?php include 'image_header.php' ?>

  <?php 
  if(!isset($_GET['halaman']))
  {
    include 'home.php';
  }
  else
  {
    if ($_GET['halaman']=="register") 
    {
      include 'penjual/registrasi/register.php';
    }
    elseif($_GET['halaman']=="data_lengkap")
    {
     include 'penjual/registrasi/data_lengkap.php';
   }
   elseif($_GET['halaman']=="info")
   {
    include 'info.php';
  }
  elseif($_GET['halaman']=="pemberitahuan")
  {
    include 'penjual/registrasi/pemberitahuan.php';
  }
  elseif($_GET['halaman']=="hasil_produk")
  {
    include 'hasil_produk.php';
  }
  elseif($_GET['halaman']=="kategori_produk")
  {
    include 'kategori.php';
  }
  elseif($_GET['halaman']=="home")
  {
    include 'home.php';
  }
}

?>
</body>
<footer>
	<?php include 'footer.php' ?>
</footer>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="asset/js/bootstrap.min.js"></script>
<script src="asset/OwlCarousel/dist/owl.carousel.min.js"></script>
<script>
  $(document).ready(function(){
    $(".owl-carousel").owlCarousel({
      items:1,
      loop : true,
      autoplay:5,
    });
  });
</script>
</html>